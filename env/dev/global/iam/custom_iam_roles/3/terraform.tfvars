target_level = "project"
target_id    = "searce-playground-v1"
role_id      = "prod_gke_apps_role"
title        = "prod_gke_apps_role"
base_roles   = []
permissions  = ["storage.buckets.get","storage.buckets.list","storage.buckets.getIamPolicy","storage.buckets.setIamPolicy","storage.buckets.update","storage.objects.create","storage.objects.get","storage.objects.list","resourcemanager.projects.get","secretmanager.versions.access"]
excluded_permissions = []
members      = []
