target_level = "project"
target_id    = "searce-playground-v1"
role_id      = "prod_cloud_build_shubham_role"
title        = "prod_cloud_build_shubham_role"
base_roles   = ["roles/iam.serviceAccountUser","roles/logging.logWriter","roles/storage.admin","roles/run.admin","roles/secretmanager.secretAccessor","roles/artifactregistry.writer"]
permissions  = []
excluded_permissions = ["resourcemanager.projects.list"]
members      = []
