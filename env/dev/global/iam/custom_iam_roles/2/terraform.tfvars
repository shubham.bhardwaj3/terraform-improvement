target_level = "project"
target_id    = "searce-playground-v1"
role_id      = "prod_gke_cluster_role"
title        = "prod_gke_cluster_role"
base_roles   = []
permissions  = ["logging.logEntries.create","monitoring.metricDescriptors.create","monitoring.metricDescriptors.get","monitoring.metricDescriptors.list","monitoring.monitoredResourceDescriptors.get","monitoring.monitoredResourceDescriptors.list","monitoring.timeSeries.create","monitoring.timeSeries.list","resourcemanager.projects.get","storage.objects.get","storage.objects.list"]
excluded_permissions = []
members      = []
