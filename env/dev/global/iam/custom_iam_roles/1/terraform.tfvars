target_level = "project"
target_id    = "searce-playground-v1"
role_id      = "prod_infra_mgmt_gce_role"
title        = "prod_infra_mgmt_gce_role"
base_roles   = ["roles/compute.admin","roles/compute.networkAdmin","roles/compute.storageAdmin","roles/container.admin","roles/iam.roleAdmin","roles/iam.serviceAccountAdmin","roles/iam.serviceAccountKeyAdmin","roles/iam.serviceAccountUser","roles/resourcemanager.projectIamAdmin","roles/storage.admin"]
permissions  = []
excluded_permissions = ["compute.firewallPolicies.addAssociation","compute.firewallPolicies.copyRules","compute.firewallPolicies.move","compute.firewallPolicies.removeAssociation","compute.organizations.administerXpn","compute.organizations.disableXpnHost","compute.organizations.disableXpnResource","compute.organizations.enableXpnHost","compute.organizations.enableXpnResource","compute.organizations.listAssociations","compute.organizations.setFirewallPolicy","compute.organizations.setSecurityPolicy","compute.oslogin.updateExternalUser","compute.securityPolicies.addAssociation","compute.securityPolicies.copyRules","compute.securityPolicies.move","compute.securityPolicies.removeAssociation","resourcemanager.projects.list"]
members      = []
