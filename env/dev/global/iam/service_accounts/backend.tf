terraform {
  backend "gcs" {
    bucket = "shubham-tfstate-backend-gcs"
    prefix = "terrafrom-improvement/env/dev/global/iam/service_accounts"
  }
}

