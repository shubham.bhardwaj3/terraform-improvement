module "service_accounts" {
  source        = "../../../../../modules/iam/service_accounts/service_account_creation"
  project_id    = var.project_id
  prefix        = var.prefix
  names         = var.names
  project_roles = var.project_roles
# display_name  = var.display_name
  descriptions   = var.descriptions
}

module "service_account-iam-bindings" {
  source = "../../../../../modules/iam/service_accounts/service_account_role_bindings"

  service_accounts = module.service_accounts.emails_list
  project          = var.project_id
  mode             = "additive"
  bindings = {
    "roles/dev_infra_mgmt_gce_role" = [
      "serviceAccount:${module.service_accounts.emails_list[0]}",
    ]

    "roles/dev_gke_cluster_role" = [
      "serviceAccount:${module.service_accounts.emails_list[1]}",
    ]
     "roles/dev_gke_apps_role" = [
      "serviceAccount:${module.service_accounts.emails_list[2]}",
    ]
    "roles/prod_cloud_build_shubham_role" = [
      "serviceAccount:${module.service_accounts.emails_list[3]}",
    ]


  }
  conditional_bindings = []
  depends_on = [module.service_accounts]
}
