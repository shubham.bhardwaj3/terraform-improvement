/* GLOBAL */
project_id = "searce-playground-v1"
region     = "asia-south1"
zone = "asia-south1-a"
name                            = "shubham-cloudsql-tfi-test2"
tier                            = "db-custom-1-3840"
database_version                = "MYSQL_5_6"
availability_type               = "ZONAL"
disk_size                       = 10
disk_autoresize                 = false
disk_type                       = "PD_HDD"
maintenance_window_day          = 6
maintenance_window_hour         = 22
maintenance_window_update_track = "stable"
deletion_protection = false
user_labels = { env = "dev", user = "shubham-bhardwaj"}
database_flags = []
backup_configuration = {
  enabled                        = true
  binary_log_enabled             = true
  start_time                     = "22:00"
  location                       = "asia-south1"
  point_in_time_recovery_enabled = false
  transaction_log_retention_days = null
  retained_backups               = 1
  retention_unit                 = "COUNT"
}
ip_configuration = {
  ipv4_enabled        = true
  require_ssl         = false
  private_network     = "projects/searce-playground-v1/global/networks/saurabh-team-vpc"
  authorized_networks = []

}
read_replicas = [
  {
    name                = "shubham-cloudsql-tfi-test1-readreplica"
    zone                = "asia-south1-b"
    tier                = "db-custom-1-3840"
    disk_autoresize     = false
    disk_size           = 10
    disk_type           = "PD_SSD"
    user_labels         = { env = "dev", type = "replica"}
    encryption_key_name = null
    retained_backups    = 0
    database_flags      = []
    ip_configuration = {
      ipv4_enabled        = true
      require_ssl         = false
      private_network     = "projects/searce-playground-v1/global/networks/saurabh-team-vpc"
      authorized_networks = []
    }
  }
]
/*
insights_config = {
  query_string_length     = 1024
  record_application_tags = true
  record_client_address   = true
}
*/

