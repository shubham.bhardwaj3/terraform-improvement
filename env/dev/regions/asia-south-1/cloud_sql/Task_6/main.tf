module "shubham-test-mysql" {
  source = "../../../../../../modules/cloudsql/postgresql"
  project_id           = var.project_id
  region               = var.region
  random_instance_name = false
  name                 = var.name
  database_version     = var.database_version
  deletion_protection  = var.deletion_protection

  // Master configurations
  tier                            = var.tier
  zone                            = var.zone
  availability_type               = var.availability_type
  ip_configuration                = var.ip_configuration
  disk_size                       = var.disk_size
  disk_autoresize                 = var.disk_autoresize
  disk_type                       = var.disk_type
  maintenance_window_day          = var.maintenance_window_day
  maintenance_window_hour         = var.maintenance_window_hour
  maintenance_window_update_track = var.maintenance_window_update_track
  database_flags = var.database_flags
  user_labels = var.user_labels
  backup_configuration = var.backup_configuration
  read_replicas   = var.read_replicas
  insights_config = var.insights_config
}
