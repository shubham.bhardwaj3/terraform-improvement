terraform {
  backend "gcs" {
    bucket = "shubham-tfstate-backend-gcs"
    prefix = "terraform-practice/env/dev/regions/asia-south1/vpc"
  }
}
