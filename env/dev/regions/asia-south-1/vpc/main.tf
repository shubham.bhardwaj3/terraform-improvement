module "shubham-vpc-module" {
  source       = "../../../../../modules/networking/terraform-google-network/"
  project_id   = var.project_id 
  network_name = var.network_name
  mtu          = 1460
  subnets = var.subnets
  secondary_ranges = var.secondary_ip_ranges 
}
