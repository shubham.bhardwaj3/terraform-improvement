# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.25.0"
  constraints = ">= 2.12.0, >= 3.45.0, >= 3.83.0, ~> 4.0, < 5.0.0"
  hashes = [
    "h1:1GdTUn3fADCQZj28dEjI4s4/tUvqG8Y4Yl08K1yiV5E=",
    "zh:14da503319505637771c58cf92c513564d796bcb54000e757ae653edb750cb99",
    "zh:338823bf9e40f062a3e029f7661ee6d489712d42aaeaa8b08834f600abd96bdb",
    "zh:477a544129224af2524a1641911c16fd7e0dab0b5d5cfa3a98b71e2f078d2cd3",
    "zh:5d1f4f438c61348ed699d3686bff9ad3995eef255c4bccf9ea22cbd1ad6a0077",
    "zh:61c30019369e748e4c268a8b011e7568a2079ecf0b0d6ad44f16ea448b898934",
    "zh:91dbe4bb0585853ed7f07fb1e2dcd94b16ac5c09a1b39029f0e49ac6ba7ced2f",
    "zh:99976daec2602a8ddad313f1eb3f22eee930718a8c45130b9e46cbf3e572c257",
    "zh:adafe0c88a7f6ac6fce74beac58dda5c04c25e5964d2fc2349724eb7cfb3d834",
    "zh:b8b90360a69567572d064edd476b77ad9779d88aace195e9b5ff7b774b9dd370",
    "zh:ce21902dc8d07e7add99e1cbcff1f79227d3b52b8cab20c0bf12fa8fbad30fa9",
    "zh:e728e296c37bc445d1a60a9f7d96a422f260182f0bf8851c9038982c343c01a4",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/google-beta" {
  version     = "4.25.0"
  constraints = ">= 3.45.0, < 5.0.0"
  hashes = [
    "h1:0HUgs7Ipcy4XoQo0vrXpe4kjK3rppIO6p+FzZLCZm4c=",
    "zh:07485fb415f17ff0f0cd75cb86bf2bc920b498da5176b7ba92e0e48606539cd3",
    "zh:31a0b3f90c0cd9e3c6ca8746a6e34d307cda7782a4e6ffa8b03aeb19f64984a6",
    "zh:61c7b5b8a9f928ac8f5d8763d07baffd7a6512e9f8d095f2fa9249cb6c93e259",
    "zh:6dde8d6b42a844de0fa9d4f26a92eea4198efc6344ebb863e45deeb5febd13ef",
    "zh:79aecd31b3d0c970d57fedbce4c3ac5881ba65c0cd32beaaa4fcfb0d6f73c5f2",
    "zh:8478366c6bf06caf34ce3bd6801e53f188ec7b9c79af6937cdb486c275ed2711",
    "zh:96daa4515f6b3b48323153ac692d71ecb427e95bf42bf5b35f65f6935378e31d",
    "zh:bd626e168a3e1db21730dac7b98cb2ffd98c4cdd32272022166cfb62f454001c",
    "zh:c5aef8617cbbd1b65d3674657997cf37af5ace8350aeb321122f4a5d209a092b",
    "zh:d09d9eddd0dcf01c59148fe33a2c65b6b32a1216a2d2c473bb05c391a3e27732",
    "zh:e230299f41d6c211173ba933efc1d235f5385305b1ea005a7ecf24af255a756e",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version     = "2.1.2"
  constraints = "~> 2.1"
  hashes = [
    "h1:aPFPXMBQRTBTrEwAOroPo/ruSZoVd2CVHdakT0C0NwQ=",
    "zh:0cc7236c1fbf971b8bad1540a7d0c5ac4579248239fd1034c023b0b660a8d1d5",
    "zh:16fc2d9b10cf9e5123bf956e7032c338cc93a03be1ca2e9f3d3b7014c0e866c7",
    "zh:1e26465ff40ded59cef1a9e745752eef9744471e69094d12f8bc40a060e8cdb9",
    "zh:3c7afd28076245f455d4348af6c124b73409722be4a73680d4e4709a4f25ea91",
    "zh:4190a92567efaa444527f19b28d65fac1a01aeba907013b5dceacd9ba2a23709",
    "zh:677963437316b088fc1aac70fe7d608d2917c46530c4a25ec86a43e9acaf2811",
    "zh:69fe15f6b851ff82700bc749c50a9299770515617e677c18cba2cb697daaff36",
    "zh:6b505cc60cc1494e1cab61590bca127b06dd894d0b2a7dcacd23862bce1f492b",
    "zh:719aa11ad7be974085af595089478eb24d5a021bc7b08364fa6745d9eb473dac",
    "zh:7375b02189e14efbfaab994cd05d81e3ff8e46041fae778598b3903114093a3e",
    "zh:c406573b2084a08f8faa0451923fbeb1ca6c5a5598bf039589ec2db13aacc622",
    "zh:fb11299a3b20711595713d126abbbe78c554eb5995b02db536e9253686798fb6",
  ]
}
