variable "project_id" {
  description = "The project ID to host the network in"
}

variable "network_name" {
  description = "The name of the VPC network being created"
}

variable "subnets" {
  description = "The details of subnets to be created"
}
variable "secondary_ip_ranges" {
  description = "The details of secondary_ip_range to be created"
}
