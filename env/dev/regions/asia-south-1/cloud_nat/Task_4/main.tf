module "cloud-nat" {
  source     = "../../../../../../modules/networking/terraform-google-cloud-nat"
  router     = data.terraform_remote_state.cloud_router.outputs.router_name
  project_id = var.project_id
  region     = var.region
  min_ports_per_vm                   = "128"
  icmp_idle_timeout_sec              = "15"
  tcp_established_idle_timeout_sec   = "600"
  tcp_transitory_idle_timeout_sec    = "15"
  udp_idle_timeout_sec               = "15"
  name       = "shubham-cloud-tfi-nat"
}
