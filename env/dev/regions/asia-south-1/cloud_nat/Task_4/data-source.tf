data "terraform_remote_state" "cloud_router" {
  backend = "gcs"
  config = {
    bucket = "shubham-tfstate-backend-gcs"
    prefix = "terrafrom-improvement/env/dev/regions/asia-south-1/cloud_router/Task_4"
  }
}
